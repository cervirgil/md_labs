package ro.mta.dm.mdlabs.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import ro.mta.dm.mdlabs.R;
import ro.mta.dm.mdlabs.utils.Constants;
import ro.mta.dm.mdlabs.utils.Utils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_RECEIVE_SMS = 3;
    private final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e(TAG, savedInstanceState == null ? "IT IS NULL" : "NOT NULL");

        Log.d(TAG, "OnCreate");
        setContentView(R.layout.activity_main);

        findViewById(R.id.main_button_primary).setOnClickListener(this);
        findViewById(R.id.main_button_secondary).setOnClickListener(this);
        findViewById(R.id.main_button_map).setOnClickListener(this);

        checkDynamicPermission();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d(TAG, "OnStart");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "OnResume");
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d(TAG, "OnPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d(TAG, "OnStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "OnDestroy");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "OnSaveInstanceState");
        outState.putString(TAG, "My Saved Item");

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        Log.d(TAG, "OnRestoreInstanceState");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_button_primary: {
                Utils.showToast(this, "Main Button Pushed");

                Log.d(TAG, "On Button Clicked");

                startSecondActivity(v.getId());
                break;
            }
            case R.id.main_button_secondary: {
                Log.d(TAG, "On Second Button click");

                startSecondActivity(v.getId());
                break;
            }
            case R.id.main_button_map: {
                Log.d(TAG, "Starting MapActivity");
                Intent intent = new Intent(this, MapsActivity.class);
                startActivity(intent);
                break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_RECEIVE_SMS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Log.d(TAG, "PERMISSION GRANTED: " + (permissions != null && permissions.length > 0 ? permissions[0] : ""));
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.d(TAG, "PERMISSION DENIED: " + (permissions != null && permissions.length > 0 ? permissions[0] : ""));
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    /**
     * Method used for starting the {@link SecondActivity}
     *
     * @param buttonId The id of the button used for starting the activity
     */
    private void startSecondActivity(int buttonId) {
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(Constants.SECOND_ACTIVITY_PARAM_KEY, buttonId);
        startActivity(intent);
    }

    private void checkDynamicPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECEIVE_SMS)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Log.d(TAG, "SHOW DETAILS");
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECEIVE_SMS}, MY_PERMISSIONS_REQUEST_RECEIVE_SMS);

                // MY_PERMISSIONS_REQUEST_RECEIVE_SMS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
}