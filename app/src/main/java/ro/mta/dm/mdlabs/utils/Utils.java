package ro.mta.dm.mdlabs.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by john on 16/03/16.
 * <p>
 * Class for centralizing methods useful across the app
 */
public class Utils {

    private static Toast mToast;

    /**
     * Shows a toast using a predefined String ID, canceling the current toast if existent.
     *
     * @param context Activity context
     * @param content String Id (strings.xml)
     */
    public static void showToast(Context context, int content) {
        if (mToast == null) {
            mToast = Toast.makeText(context, content, Toast.LENGTH_SHORT);
        }

        mToast.setText(content);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.show();
    }

    /**
     * Shows a toast using the given content string, canceling the current toast if existent.
     *
     * @param context Activity context
     * @param content Toast's String content
     */
    public static void showToast(Context context, CharSequence content) {
        if (mToast == null) {
            mToast = Toast.makeText(context, content, Toast.LENGTH_SHORT);
        }

        mToast.setText(content);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.show();
    }
}
