package ro.mta.dm.mdlabs.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import ro.mta.dm.mdlabs.R;
import ro.mta.dm.mdlabs.adapters.StudentsAdapter;
import ro.mta.dm.mdlabs.fragments.abstracts.FragmentStateCallback;
import ro.mta.dm.mdlabs.models.Student;
import ro.mta.dm.mdlabs.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentStateCallback} interface to handle interaction events.
 * <p/>
 * Use the {@link ListSampleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListSampleFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private final String TAG = getClass().getSimpleName();
    private String mParam1;

    private FragmentStateCallback mListener;

    public ListSampleFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment ListSampleFragment.
     */
    public static ListSampleFragment newInstance(String param1) {
        ListSampleFragment fragment = new ListSampleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mainView = inflater.inflate(R.layout.fragment_list_sample, container, false);

        Log.v(TAG, "onCreateView");

        // Load elements
        ListView listView = (ListView) mainView.findViewById(R.id.fragment_list_sample);

        // Simple string list adapter
        /*ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.sample_list));
        listView.setAdapter(stringArrayAdapter);*/

        // Complex Student ArrayAdapter
        List<Student> mockStudents = new ArrayList<>();

        mockStudents.add(new Student("100", "Vasile", "Gheorghe", "E214A", "http://cdn.1001freedownloads.com/icon/thumb/396419/black-spider-icon.png"));
        mockStudents.add(new Student("101", "Ion", "Vasile", "E214B", "http://cdn-img.easyicon.net/png/11901/1190176.gif"));
        mockStudents.add(new Student("102", "Gheorghe", "Ion", "E214C", "http://orig06.deviantart.net/97b5/f/2010/122/3/b/punisher_logo_by_abaworlock.jpg"));

        StudentsAdapter studentsAdapter = new StudentsAdapter(getContext(), R.layout.item_student, mockStudents);
        listView.setAdapter(studentsAdapter);

        return mainView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentStateCallback) {
            mListener = (FragmentStateCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement " + FragmentStateCallback.class.getSimpleName());
        }

        mListener.onFragmentAttached(Constants.FRAGMENT_LIST_UID);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener.onFragmentDetached(Constants.FRAGMENT_LIST_UID);
        mListener = null;
    }

}
