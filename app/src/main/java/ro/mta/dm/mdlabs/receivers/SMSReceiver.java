package ro.mta.dm.mdlabs.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import ro.mta.dm.mdlabs.activities.SecondActivity;
import ro.mta.dm.mdlabs.utils.Utils;

/**
 * Created by Virgil on 22/03/16.
 * <p/>
 * Receiver called when an SMS is received
 */
public class SMSReceiver extends BroadcastReceiver {

    private final String TAG = getClass().getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs;
        Log.e(TAG, "SMS: " + intent.getAction().equalsIgnoreCase("android.provider.Telephony.SMS_RECEIVED"));

        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus != null ? pdus.length : 0];

            String content = "";
            String receiverPhoneNumber = "";
            for (int i = 0; i < msgs.length; i++) {
                if (pdus != null && i < pdus.length) {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    receiverPhoneNumber = msgs[i].getOriginatingAddress();
                    content += msgs[i].getMessageBody();
                }
            }

            Log.e(TAG, "RECEIVED SMS " + content + " from " + receiverPhoneNumber);
            Utils.showToast(context, receiverPhoneNumber + ": " + content);

            // Start an activity
            Intent startSecondActivity = new Intent(context, SecondActivity.class);
            startSecondActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(startSecondActivity);
        }
    }
}
