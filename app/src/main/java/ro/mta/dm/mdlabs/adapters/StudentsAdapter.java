package ro.mta.dm.mdlabs.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ro.mta.dm.mdlabs.R;
import ro.mta.dm.mdlabs.models.Student;

/**
 * Created by john on 16/03/16.
 * <p/>
 * Adapter for students list
 */
public class StudentsAdapter extends ArrayAdapter<Student> {

    private List<Student> students;

    public StudentsAdapter(Context context, int resourceId, List<Student> students) {
        super(context, resourceId);

        this.students = students;
    }

    @Override
    public int getCount() {
        if (students != null) {
            return students.size();
        } else {
            return 0;
        }
    }

    @Override
    public Student getItem(int position) {
        if (students != null && position < students.size()) {
            return students.get(position);
        }

        return null;
    }

    @Override
    public long getItemId(int position) {
        if (students != null && position < students.size() && !TextUtils.isEmpty(students.get(position).getId())) {
            return students.get(position).getId().hashCode();
        }

        return -1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Student student = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_student, parent, false);
            viewHolder = new ViewHolder(convertView);

            // Cache the loading of the elements
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (student != null) {
            // Populate the view
            viewHolder.name.setText(String.format("%s %s", student.getFirstName(), student.getLastName()));
            viewHolder.group.setText(student.getGroup());

            // Download the profile image
            if (!TextUtils.isEmpty(student.getProfileImageURL())) {
                Glide.with(getContext())
                        .load(student.getProfileImageURL())
                        .fitCenter()
                        .placeholder(R.drawable.logo_batman)
                        .crossFade()
                        .into(viewHolder.profile);
            }
        } else {
            viewHolder.name.setText(R.string.error_null);
            viewHolder.group.setText(R.string.error_null);
        }

        // Return the completed view to render on screen
        return convertView;
    }

    // View lookup cache
    public static class ViewHolder {
        @Bind(R.id.item_student_name)
        TextView name;

        @Bind(R.id.item_student_group)
        TextView group;

        @Bind(R.id.item_student_profile)
        ImageView profile;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
