package ro.mta.dm.mdlabs.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by Virgil on 22/03/16.
 * <p/>
 * Receiver called when there was a change in the network connection
 */
public class NetworkStateReceiver extends BroadcastReceiver {

    private final String TAG = getClass().getSimpleName();

    @Override
    public void onReceive(final Context context, final Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        boolean isWiFi = activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        boolean isMobile = activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;

        if (isConnected && (isWiFi || isMobile)) {
            Log.i(TAG, "Network connected");
        } else {
            Log.w(TAG, "Network disconnected");
        }
    }
}