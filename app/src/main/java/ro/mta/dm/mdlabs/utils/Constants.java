package ro.mta.dm.mdlabs.utils;

/**
 * Created by john on 16/03/16.
 * <p>
 * Class used to store constants necessary accross the app
 */
public class Constants {

    public static final String SECOND_ACTIVITY_PARAM_KEY = "SecondActivityStartParam";

    // Fragment UIDs used to register with the owning activities
    public static final int FRAGMENT_BUTTONS_UID = 1;
    public static final int FRAGMENT_IMAGE_UID = 2;
    public static final int FRAGMENT_LIST_UID = 3;
}
