package ro.mta.dm.mdlabs.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ro.mta.dm.mdlabs.R;
import ro.mta.dm.mdlabs.fragments.abstracts.FragmentStateCallback;
import ro.mta.dm.mdlabs.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the {@link FragmentStateCallback} interface
 * to handle interaction events.
 * <p/>
 * Use the {@link ImageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImageFragment extends Fragment {
    private static final String ARG_START_BUTTON_ID = "StartingButtonId";
    private final String TAG = getClass().getSimpleName();
    /*The id of the button that started the fragment */
    private int mStartButtonId;

    private FragmentStateCallback mListener;

    private int mImageSrc = 0;

    public ImageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param startButtonId The id of the button that started this fragment
     * @return A new instance of fragment ImageFragment.
     */
    public static ImageFragment newInstance(int startButtonId) {
        ImageFragment fragment = new ImageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_START_BUTTON_ID, startButtonId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mStartButtonId = getArguments().getInt(ARG_START_BUTTON_ID, -1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mainView = inflater.inflate(R.layout.fragment_image, container, false);

        // Load elements
        TextView startLabel = (TextView) mainView.findViewById(R.id.fragment_image_start_label);

        // Setup image view source in accordance with the button that started the fragment
        final ImageView imageView = (ImageView) mainView.findViewById(R.id.fragment_image_imageview);

        int imageViewResourceId = R.drawable.logo_mta;
        switch (mStartButtonId) {
            case R.id.fragment_buttons_insert_window_button: {
                startLabel.setText(getString(R.string.fragment_image_start_label, getString(R.string.fragment_buttons_insert_button)));
                imageViewResourceId = R.drawable.logo_batman;
                break;
            }
            case R.id.fragment_buttons_new_window_button: {
                startLabel.setText(getString(R.string.fragment_image_start_label, getString(R.string.fragment_buttons_new_window_button)));
                imageViewResourceId = R.drawable.logo_superman;
                break;
            }
            default: {
                Log.e(TAG, "Unknown button started the fragment");
                startLabel.setText(getString(R.string.fragment_image_start_label, getString(R.string.error_unknown)));
                break;
            }
        }
        imageView.setImageResource(imageViewResourceId);

        mainView.findViewById(R.id.fragment_image_switch_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int imageViewResourceId;
                switch (mImageSrc % 3) {
                    case 0: {
                        imageViewResourceId = R.drawable.logo_mta;
                        break;
                    }
                    case 1: {
                        imageViewResourceId = R.drawable.logo_superman;
                        break;
                    }
                    default: {
                        imageViewResourceId = R.drawable.logo_batman;
                        break;
                    }
                }
                mImageSrc = ++mImageSrc % 3;
                imageView.setImageResource(imageViewResourceId);
            }
        });

        mainView.findViewById(R.id.fragment_image_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        return mainView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentStateCallback) {
            mListener = (FragmentStateCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement FragmentStateCallback");
        }

        mListener.onFragmentAttached(Constants.FRAGMENT_IMAGE_UID);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener.onFragmentDetached(Constants.FRAGMENT_IMAGE_UID);
        mListener = null;
    }

}
