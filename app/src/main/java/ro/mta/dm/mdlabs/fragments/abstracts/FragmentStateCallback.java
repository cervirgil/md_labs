package ro.mta.dm.mdlabs.fragments.abstracts;

/**
 * Created by john on 16/03/2016.
 * <p/>
 * Interface that all activities with fragments should implement in order to be notified
 * on the events in the fragment's lifecycle
 */
public interface FragmentStateCallback {

    /**
     * Notifies the activity in order to change the title, and the menu displayed in the action bar
     */
    void onFragmentAttached(int fragmentUID);

    /**
     * Notifies the activity in order to change the title, and the menu displayed in the action bar
     */
    void onFragmentDetached(int fragmentUID);
}