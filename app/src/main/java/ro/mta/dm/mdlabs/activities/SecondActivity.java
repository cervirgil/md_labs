package ro.mta.dm.mdlabs.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import ro.mta.dm.mdlabs.R;
import ro.mta.dm.mdlabs.fragments.ButtonsFragment;
import ro.mta.dm.mdlabs.fragments.abstracts.FragmentStateCallback;
import ro.mta.dm.mdlabs.utils.Constants;
import ro.mta.dm.mdlabs.utils.Utils;

public class SecondActivity extends AppCompatActivity implements FragmentStateCallback {

    // Logging tag
    private final String TAG = SecondActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        // Verify if the intent that started this activity has an extra
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(Constants.SECOND_ACTIVITY_PARAM_KEY)) {
            int buttonId = extras.getInt(Constants.SECOND_ACTIVITY_PARAM_KEY, -1);
            String message;

            switch (buttonId) {
                case R.id.main_button_primary: {
                    message = getString(R.string.main_button_primary);
                    break;
                }
                case R.id.main_button_secondary: {
                    message = getString(R.string.main_button_secondary);
                    break;
                }
                default: {
                    message = getString(R.string.error_unknown);
                    break;
                }
            }

            Utils.showToast(this, getString(R.string.second_start_intent, message));
        }

        initUI();
    }

    @Override
    public void onFragmentAttached(int fragmentUID) {
        Log.d(TAG, "Fragment attached: " + fragmentUID);
    }

    @Override
    public void onFragmentDetached(int fragmentUID) {
        Log.d(TAG, "Fragment detached: " + fragmentUID);
    }

    private void initUI() {
        // Load the buttons fragment
        Fragment buttonsFragment = ButtonsFragment.newInstance("Example param");

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.second_fragment_container, buttonsFragment, "TAG" + Constants.FRAGMENT_BUTTONS_UID)
                .commit();
    }
}
