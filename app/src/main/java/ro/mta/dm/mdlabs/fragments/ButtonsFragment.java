package ro.mta.dm.mdlabs.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ro.mta.dm.mdlabs.R;
import ro.mta.dm.mdlabs.fragments.abstracts.FragmentStateCallback;
import ro.mta.dm.mdlabs.utils.Constants;
import ro.mta.dm.mdlabs.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the {@link FragmentStateCallback} interface
 * to handle interaction events.
 * <p/>
 * Use the {@link ButtonsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ButtonsFragment extends Fragment implements View.OnClickListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private final String TAG = ButtonsFragment.class.getSimpleName();
    private String mParam1;

    private FragmentStateCallback mListener;

    private Button mColorButton;

    private int mColorFlag = 0;

    public ButtonsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment ButtonsFragment.
     */
    public static ButtonsFragment newInstance(String param1) {
        ButtonsFragment fragment = new ButtonsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mainView = inflater.inflate(R.layout.fragment_buttons, container, false);
        Log.d(TAG, "Fragment OnCreateView with param " + mParam1);

        // Init elements
        mColorButton = (Button) mainView.findViewById(R.id.fragment_buttons_colored_button);

        // Set click listeners
        mainView.findViewById(R.id.fragment_buttons_colored_button).setOnClickListener(this);
        mainView.findViewById(R.id.fragment_buttons_simple_button_1).setOnClickListener(this);
        mainView.findViewById(R.id.fragment_buttons_simple_button_2).setOnClickListener(this);
        mainView.findViewById(R.id.fragment_buttons_insert_window_button).setOnClickListener(this);
        mainView.findViewById(R.id.fragment_buttons_new_window_button).setOnClickListener(this);

        return mainView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof FragmentStateCallback) {
            mListener = (FragmentStateCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }

        mListener.onFragmentAttached(Constants.FRAGMENT_BUTTONS_UID);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.onFragmentDetached(Constants.FRAGMENT_BUTTONS_UID);
    }

    @Override
    public void onClick(View v) {
        showButtonText(v);

        switch (v.getId()) {
            case R.id.fragment_buttons_colored_button: {
                mColorButton.setBackgroundResource(mColorFlag % 2 == 0 ? R.drawable.selector_green_dark : R.drawable.selector_orange_dark);
                mColorFlag = ++mColorFlag % 2;
                break;
            }
            case R.id.fragment_buttons_new_window_button: {
                showListFragment();
                break;
            }
            case R.id.fragment_buttons_insert_window_button: {
                showImageFragment(v.getId());
                break;
            }
        }
    }

    /**
     * Shows a toast with the view's text, in the case where it is a {@link Button}
     *
     * @param v The button view
     */
    private void showButtonText(View v) {
        Button button;

        try {
            button = (Button) v;
        } catch (Exception e) {
            Log.e(TAG, "Failed to get button: " + e.getMessage());
            return;
        }

        Utils.showToast(getContext(), button.getText());
    }

    /**
     * Shows the {@link ImageFragment} in the fragment container
     *
     * @param buttonId The id of the button that was pressed
     */
    private void showImageFragment(int buttonId) {
        Fragment imageFragment = ImageFragment.newInstance(buttonId);

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.second_fragment_container, imageFragment, "TAG" + Constants.FRAGMENT_IMAGE_UID)
                .addToBackStack("BackStack" + Constants.FRAGMENT_IMAGE_UID)
                .commit();
    }

    private void showListFragment() {
        Fragment listFragment = ListSampleFragment.newInstance("URL for getting new elements for example");

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.second_fragment_container, listFragment, "TAG" + Constants.FRAGMENT_LIST_UID)
                .addToBackStack("BackStack" + Constants.FRAGMENT_LIST_UID)
                .commit();
    }
}
